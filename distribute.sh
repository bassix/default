#!/bin/bash

## Build adminer.php

cd adminer
make initialize
make compile
mv adminer.php ../public/
cd ..

## Copy to targets

if [ ! -f ".distribute" ]; then
    echo "The \".distribute\" file does not exist!"
    exit 1
fi

# @todo MacOS is not on bash 4.0 and function doesn't exist!
#mapfile -t targets < .distribute
#readarray -t targets < .distribute
targets=($(cat ".distribute"))

for target in "${targets[@]}"
do
    if [ -d "$target" ]
    then
        read -p "Do you want to run \"rsync -avhr --delete ./public/ $target\" (y/n)? " doRsync
        if [ "$doRsync" == "y" ]
        then
            rsync -avhr --delete ./public/ "$target"
        else
            echo "rsync to \"$target\" aborted!"
        fi
    else
        echo "The configured target \"$target\" does not exist!"
    fi
done
