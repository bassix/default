<div class="login-page">
    <div class="form">
        <?php

        switch ($_REQUEST['part'] ?? 'login') {
            case 'register':
                include 'part/register.php';
                break;
            default:
                include 'part/login.php';
        }

        ?>
    </div>
</div>
