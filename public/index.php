<?php

$page = $_REQUEST['page'] ?? 'auth';

?><!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta charset="utf-8">
    <title>Login</title>
    <meta name="robots" content="noindex,nofollow">
    <link rel="icon" href="favicon.svg" type="image/svg+xml">
    <link rel="alternate icon" href="favicon.ico" type="image/x-icon">
    <link rel="shortcut icon" href="favicon.ico" type="image/x-icon">
    <link rel="apple-touch-icon" href="apple-touch-icon.png" sizes="180x180">
    <link rel="icon" href="favicon-32x32.png" type="image/png" sizes="32x32">
    <link rel="icon" href="favicon-16x16.png" type="image/png" sizes="16x16">
    <link rel="stylesheet" href="css/main.css">
</head>
<body>
<?php

if (file_exists("inc/$page.php")) {
    include "inc/$page.php";
} else {
    include 'inc/404.php';
}

?></body>
</html>
