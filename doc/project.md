# Project

Working with two reposiotries:

```shell
git remote set-url origin --push --add git@github.com:bassix/default.git
git remote set-url origin --push --add git@gitlab.com:bassix/default.git
git remote update
git push --all
```

## Adminer

The Adminer is installed as a submodule:

```shell
git submodule add git@github.com:vrana/adminer.git adminer
git submodule update --init
```
