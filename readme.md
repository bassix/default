# bassix default

**This repository holds a simple project for a default page on an empty webserver with php enabled.**

To get involved into the development of this project you need to get a local copy of this repository:

```shell
# The GitLab repository
git clone git@gitlab.com:bassix/default.git bassix-default
# The GitHub repository
git clone git@github.com:bassix/default.git bassix-default
cd bassix-default
```

## Distribute

Before build and distribute the targets inside `.distribute` should be prepared:

```shell
echo "/var/www/html" >> .distribute
```

Now run the distribution script:

```shell
./distribute.sh
```

### Manual Distribution

Following steps are executed inside the distribution script:

1. Build `adminer.php`:

    ```shell
    cd adminer
    make initialize
    make compile
    mv adminer.php ../public/
    cd ..
   ```

1. Copy the main page into place manually:

    ```shell
    rsync -avhr --delete ./public/ /var/www/html
    ```

## .htaccess

Redirect **http** to **https** with `.htaccess` file except [Let's Encrypt](https://letsencrypt.org/) the `well-known/acme-challenge` path.

With the following command the configuration can be tested:

```shell
./test.sh localhost
```

Alternative execute following steps:

* Should lead to a **404** (not found):

    ```shell
    wget --no-check-certificate -S -O/dev/null 'http://localhost/.well-known/acme-challenge/foo'
    ```

* Should lead to a **301** (permanent redirect) to the **https** host:

    ```shell
    wget --no-check-certificate -S -O/dev/null 'http://localhost/'
    ```
